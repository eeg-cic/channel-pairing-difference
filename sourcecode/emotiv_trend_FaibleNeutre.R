# rm(list=ls())
# cat("\014")

script.dir <- dirname(sys.frame(1)$ofile)
setwd(script.dir)


### import library ###
library('tidyverse')
library('stringr')
# source('multiplot.R')

### locate data folder ###
data_folder <- readline(prompt="Enter folder of data: ")
# output_name <- readline(prompt="Save output as (including.jpg): ")
output_name <- paste(data_folder,'.jpg')
folder <- list.files(data_folder)
# folder <- list.files('data')
folder <- str_to_lower(folder)





### combine condition faible into faible.csv ###
files_faible <-  list()

for (filename in folder){
  if (!is.na(str_locate(filename,'faible')[1])){
    files_faible = append(files_faible,filename)
  }
}

for (i in 1:length(files_faible)){
  if (i == 1){
    data <- read_tsv(str_c(data_folder,'\\', files_faible[i]), col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4"))
  }
  else{
    data <- bind_rows(data, read_tsv(str_c(data_folder,'\\', files_faible[i]), col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4")))
  }
}
data <- data[, colSums(is.na(data)) != nrow(data)]
write_csv(data, "faible.csv")





### combine condition intense into intense.csv ###
files_intense <-  list()

for (filename in folder){
  if (!is.na(str_locate(filename,'intense')[1])){
    files_intense = append(files_intense,filename)
  }
}

for (i in 1:length(files_intense)){
  if (i == 1){
    data <- read_tsv(str_c(data_folder,'\\', files_intense[i]), col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4"))
  }
  else{
    data <- bind_rows(data, read_tsv(str_c(data_folder,'\\', files_intense[i]), col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4")))
  }
}
data <- data[, colSums(is.na(data)) != nrow(data)]
write_csv(data, "intense.csv")





### combine condition frisson into frisson.csv ###
files_frisson <-  list()

for (filename in folder){
  if (!is.na(str_locate(filename,'frisson')[1])){
    files_frisson = append(files_frisson,filename)
  }
}

for (i in 1:length(files_frisson)){
  if (i == 1){
    data <- read_tsv(str_c(data_folder,'\\', files_frisson[i]), col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4"))
  }
  else{
    data <- bind_rows(data, read_tsv(str_c(data_folder,'\\', files_frisson[i]), col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4")))
  }
}
data <- data[, colSums(is.na(data)) != nrow(data)]
write_csv(data, "frisson.csv")




### combine condition neutre into neutre.csv ###
files_neutre <-  list()

for (filename in folder){
  if (!is.na(str_locate(filename,'neutre')[1])){
    files_neutre = append(files_neutre,filename)
  }
}

for (i in 1:length(files_neutre)){
  if (i == 1){
    data <- read_tsv(str_c(data_folder,'\\', files_neutre[i]), col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4"))
  }
  else{
    data <- bind_rows(data, read_tsv(str_c(data_folder,'\\', files_neutre[i]), col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4")))
  }
}
data <- data[, colSums(is.na(data)) != nrow(data)]
write_csv(data, "neutre.csv")






### Computation ###
faible <- read_csv('faible.csv')
intense <- read_csv('intense.csv')
frisson <- read_csv('frisson.csv')
neutre <- read_csv('neutre.csv')

# freq <- c('4-8','8-12','12-20')

faible <- (faible+neutre)/2


##########################################################################
############################ Plot faible #################################
##########################################################################

faible_tidy <-  tibble(channel = character(),
                       band = character(),
                       value = numeric())

for (i in 1:(length(faible)/2)-1){
  
  temp <- log10(faible[1+i]) - log10(faible[length(faible)-i])
  temp <- temp*-1
  label_temp <- paste(colnames(faible[1+i]),colnames(faible[length(faible)-i]),sep='-')
  
  
  for (j in 1:nrow(temp)){
    temp_tibble <- tibble(channel = label_temp,
                          band = paste('freq',j),
                          value = temp[j,1])
    faible_tidy <- bind_rows(faible_tidy, temp_tibble)
  }
}



##########################################################################
############################ Plot intense ################################
##########################################################################

intense_tidy <-  tibble(channel = character(),
                       band = character(),
                       value = numeric())

for (i in 1:(length(intense)/2)-1){
  
  temp <- log10(intense[1+i]) - log10(intense[length(intense)-i])
  temp <- temp*-1
  label_temp <- paste(colnames(intense[1+i]),colnames(intense[length(intense)-i]),sep='-')
  
  
  for (j in 1:nrow(temp)){
    temp_tibble <- tibble(channel = label_temp,
                          band = paste('freq',j),
                          value = temp[j,1])
    intense_tidy <- bind_rows(intense_tidy, temp_tibble)
  }
}




##########################################################################
############################ Plot frisson ################################
##########################################################################

frisson_tidy <-  tibble(channel = character(),
                        band = character(),
                        value = numeric())

for (i in 1:(length(frisson)/2)-1){
  
  temp <- log10(frisson[1+i]) - log10(frisson[length(frisson)-i])
  temp <- temp*-1
  label_temp <- paste(colnames(frisson[1+i]),colnames(frisson[length(frisson)-i]),sep='-')
  
  
  for (j in 1:nrow(temp)){
    temp_tibble <- tibble(channel = label_temp,
                          band = paste('freq',j),
                          value = temp[j,1])
    frisson_tidy <- bind_rows(frisson_tidy, temp_tibble)
  }
}




##########################################################################
############################ Plot neutre #################################
##########################################################################

# neutre_tidy <-  tibble(channel = character(),
#                         band = character(),
#                         value = numeric())
# 
# for (i in 1:(length(neutre)/2)-1){
#   
#   temp <- log10(neutre[1+i]) - log10(neutre[length(neutre)-i])
#   temp <- temp*-1
#   label_temp <- paste(colnames(neutre[1+i]),colnames(neutre[length(neutre)-i]),sep='-')
#   
#   
#   for (j in 1:nrow(temp)){
#     temp_tibble <- tibble(channel = label_temp,
#                           band = freq[j],
#                           value = temp[j,1])
#     neutre_tidy <- bind_rows(neutre_tidy, temp_tibble)
#   }
# }


if (file.exists('faible.csv')) 
  file.remove('faible.csv')
if (file.exists('frisson.csv')) 
  file.remove('frisson.csv')
if (file.exists('neutre.csv'))
  file.remove('neutre.csv')
if (file.exists('intense.csv')) 
  file.remove('intense.csv')


##########################################################################
###################### create output folder ##############################
##########################################################################

subDir <- paste(data_folder,'_output_FaibleNeutre',sep='')

if (file.exists(subDir)){
  setwd(file.path(script.dir, subDir))
} else {
  dir.create(file.path(script.dir, subDir))
  setwd(file.path(script.dir, subDir))
}




# neutre_tidy <- add_column(neutre_tidy, condition = 'neutre')
faible_tidy <- add_column(faible_tidy, condition = 'faible')
intense_tidy <- add_column(intense_tidy, condition = 'intense')
frisson_tidy <- add_column(frisson_tidy, condition = 'frisson')


final <-  tibble(channel = character(),
                       band = character(),
                       value = numeric(),
                       condition = character())
final <- bind_rows(final,faible_tidy,intense_tidy,frisson_tidy)

# jpeg(output_name, width = 1920, height = 1080)
# 
# p <- ggplot(final, aes(x=condition,y=value,group=band))+
#   # geom_point(aes(color = band),size=5)+
#   geom_line(aes(color = band),size=2)+
#   ggtitle(data_folder)+
#   theme(plot.title = element_text(size=22))+
#   scale_x_discrete(limits=c('neutre','faible','intense','frisson'))+
#   scale_y_continuous(breaks = pretty(final$value, n = 10))+
#   scale_color_manual(breaks = c("4-8", "8-12", "12-20"), values=c("red", "black", "blue"))+
#   facet_grid(cols = vars(channel))+
#   ylab('log10 different')+
#   theme(axis.title.y = element_text(margin = margin(t = 0, r = 20, b = 0, l = 0)))+
#   xlab('Condition')+
#   theme(axis.title.x = element_text(margin = margin(t = 20, r = 0, b = 0, l = 0)))+
#   theme(legend.position="bottom",legend.text=element_text(size=20),legend.title=element_text(size=20)) +
#   theme(axis.text.x = element_text(angle = 45, hjust = 1, size = 20),axis.text.y = element_text(hjust = 1, size = 20))+
#   theme(strip.text.x = element_text(size = 20),axis.title=element_text(size=20))
# print(p)
# dev.off()






### extract band all channel ###
extract_band <- function(freq_band){
  alpha_holder <- filter(final, final$band == freq_band)
  alpha_holder <- select(alpha_holder, -one_of('band'))
  # alpha_neutre <- filter(alpha_holder, alpha_holder$condition == 'neutre')
  alpha_faible <- filter(alpha_holder, alpha_holder$condition == 'faible')
  alpha_intense <- filter(alpha_holder, alpha_holder$condition == 'intense')
  alpha_frisson <- filter(alpha_holder, alpha_holder$condition == 'frisson')
  alpha <- alpha_faible['channel']
  # alpha <- bind_cols(alpha, alpha_neutre['value'])
  alpha <- bind_cols(alpha, alpha_faible['value'])
  alpha <- bind_cols(alpha, alpha_intense['value'])
  alpha <- bind_cols(alpha, alpha_frisson['value'])
  alpha <- rename(alpha, 'faibleneutre' = 'value', 'intense' = 'value1', 'frisson' = 'value2')
  return(alpha)
}

### extract trend channel ###
extract_trend <- function(data_band){
  step1 <- filter(data_band, data_band$intense-data_band$faibleneutre > 0)
  step2 <- filter(step1, step1$frisson-step1$intense > 0)
  return(step2)
}

for (j in 1:length(files_frisson)){
  freq_data <-  extract_band(paste('freq',j))
  write.csv(freq_data , file = paste(data_folder,'_',paste('freq',j),'.csv',sep=''))
  freq_trend <-  extract_trend(freq_data)
  write.csv(freq_trend , file = paste(data_folder,'_',paste('freq',j),'_trend','.csv',sep=''))
}

# theta <- extract_band('4-8')
# alpha <- extract_band('8-12')
# beta <- extract_band('12-20')
# write.csv(theta , file = paste(data_folder,'_theta','.csv',sep=''))
# write.csv(alpha , file = paste(data_folder,'_alpha','.csv',sep=''))
# write.csv(beta , file = paste(data_folder,'_beta','.csv',sep=''))
# 
# 
# 
# chosen_theta <- extract_trend(theta)
# chosen_alpha <- extract_trend(alpha)
# chosen_beta <- extract_trend(beta)
# write.csv(chosen_theta , file = paste(data_folder,'_trend_theta','.csv',sep=''))
# write.csv(chosen_alpha , file = paste(data_folder,'_trend_alpha','.csv',sep=''))
# write.csv(chosen_beta , file = paste(data_folder,'_trend_beta','.csv',sep=''))


